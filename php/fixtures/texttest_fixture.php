<?php

declare(strict_types=1);

require_once __DIR__ . '/../vendor/autoload.php';

use GildedRose\GildedRose;

echo 'OMGHAI!' . PHP_EOL;

$items = [
    GildedRose::type('+5 Dexterity Vest', 20, 10),
    GildedRose::type('Aged Brie', 0, 2),
    GildedRose::type('Elixir of the Mongoose', 7, 5),
    GildedRose::type('Sulfuras, Hand of Ragnaros', 80, 0),
    GildedRose::type('Sulfuras, Hand of Ragnaros', 80, -1),
    GildedRose::type('Backstage passes to a TAFKAL80ETC concert', 20, 15),
    GildedRose::type('Backstage passes to a TAFKAL80ETC concert', 49, 10),
    GildedRose::type('Backstage passes to a TAFKAL80ETC concert', 49, 5),
    GildedRose::type('Conjured Mana Cake', 6, 3),
];

$days = 2;

if (count($argv) > 1) {
    $days = (int) $argv[1];
}

for ($i = 0; $i < $days; $i++) {
    echo "-------- day ${i} --------" . PHP_EOL;
    echo 'name, sellIn, quality' . PHP_EOL;
    foreach ($items as $item) {
        if ($i != 0)
            $item->updateQuality();
        echo $item . PHP_EOL;
    }
    echo PHP_EOL;
}
