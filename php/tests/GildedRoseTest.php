<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRose;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    /**
     * @testWith ["Aged Brie", 10, 10, 11, 9]
     *           ["Aged Brie", 10, 0, 12, -1]
     *           ["Aged Brie", 10, -5, 12, -6]
     *           ["Aged Brie", 50, 5, 50, 4]
     *           ["Aged Brie", 49, 0, 50, -1]
     *           ["Aged Brie", 50, 0, 50, -1]
     *           ["Aged Brie", 50, -10, 50, -11]
     */
    public function testAgedBrie
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }

    /**
     * @testWith ["Backstage passes to a TAFKAL80ETC concert", 10, 10, 12, 9]
     *           ["Backstage passes to a TAFKAL80ETC concert", 10, 11, 11, 10]
     *           ["Backstage passes to a TAFKAL80ETC concert", 10, 5, 13, 4]
     *           ["Backstage passes to a TAFKAL80ETC concert", 10, 0, 0, -1]
     *           ["Backstage passes to a TAFKAL80ETC concert", 50, 10, 50, 9]
     *           ["Backstage passes to a TAFKAL80ETC concert", 50, 5, 50, 4]
     *           ["Backstage passes to a TAFKAL80ETC concert", 50, -5, 0, -6]
     */
    public function testBackstagePass
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }

    /**
     * @testWith ["Sulfuras, Hand of Ragnaros", 10, 10, 10, 10]
     *           ["Sulfuras, Hand of Ragnaros", 10, 0, 10, 0]
     *           ["Sulfuras, Hand of Ragnaros", 10, -1, 10, -1]
     */
    public function testSulfuras
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }

    /**
     * @testWith ["Elixir of the Mongoose", 10, 10, 9, 9]
     */
    public function testElixirOfTheMongoose
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }

    /**
     * @testWith ["+5 Dexterity Vest", 10, 10, 9, 9]
     *           ["+5 Dexterity Vest", 10, 0, 8, -1]
     *           ["+5 Dexterity Vest", 10, -1, 8, -2]
     */
    public function testDexterityVest
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }

    /**
     * @testWith ["Conjured Mana Cake", 10, 10, 8, 9]
     *           ["Conjured Mana Cake", 0, 10, 0, 9]
     *           ["Conjured Mana Cake", 10, 0, 6, -1]
     */
    public function testConjuredManaCake
    (
        string $title,
        int $quality,
        int $sell_in,
        int $expectedQuality,
        int $expectedSellIn
    )
    {
        $item = GildedRose::type($title, $quality, $sell_in);
        $item->updateQuality();
        $this->assertEquals($expectedQuality, $item->quality);
        $this->assertEquals($expectedSellIn, $item->sell_in);
    }
}
