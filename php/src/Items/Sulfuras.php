<?php

namespace GildedRose\Items;

class Sulfuras
{
    /**
     * @var int $quality
     */
    public $quality;

    /**
     * @var int $sell_in
     */
    public $sell_in;
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name, int $quality, int $sell_in)
    {
        $this->quality = $quality;
        $this->sell_in = $sell_in;
        $this->name = $name;
    }

    public function updateQuality()
    {
        return;
    }

    public function __toString(): string
    {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
}
