<?php

namespace GildedRose\Items;

class AgedBrie
{
    /**
     * @var int $quality
     */
    public $quality;

    /**
     * @var int $sell_in
     */
    public $sell_in;
    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     * @param int $quality
     * @param int $sell_in
     */
    public function __construct(string $name, int $quality, int $sell_in)
    {
        $this->quality = $quality;
        $this->sell_in = $sell_in;
        $this->name = $name;
    }

    public function updateQuality()
    {
        $this->quality += 1;
        $this->sell_in -= 1;

        if ($this->sell_in < 0) {
            $this->quality += 1;
        }

        if ($this->quality > 50) {
            $this->quality = 50;
        }
    }

    public function __toString(): string
    {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }
}
