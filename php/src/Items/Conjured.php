<?php

namespace GildedRose\Items;

class Conjured
{
    /**
     * @var int $quality
     */
    public $quality;

    /**
     * @var int $sell_in
     */
    public $sell_in;
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name, int $quality, int $sell_in)
    {
        $this->quality = $quality;
        $this->sell_in = $sell_in;
        $this->name = $name;
    }

    public function updateQuality()
    {
        $this->quality -= 2;
        $this->sell_in -= 1;

        if ($this->sell_in < 0) {
            $this->quality -= 2;
        }

        if ($this->quality < 0) {
            $this->quality = 0;
        }
    }

    public function __toString(): string
    {
        return "{$this->name}, {$this->sell_in}, {$this->quality}";
    }

}
