<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\Items\AgedBrie;
use GildedRose\Items\BackstagePass;
use GildedRose\Items\Conjured;
use GildedRose\Items\Sulfuras;

final class GildedRose
{
    public static $items = [
        'Aged Brie' => AgedBrie::class,
        'Backstage passes to a TAFKAL80ETC concert' => BackstagePass::class,
        'Sulfuras, Hand of Ragnaros' => Sulfuras::class,
        'Conjured Mana Cake' => Conjured::class,
    ];

    public static function type($name, $quality, $sell_in)
    {
        if (array_key_exists($name, self::$items))
        {
            return new self::$items[$name]($name, $quality, $sell_in);
        }
        else
        {
            return new Item($name, $sell_in, $quality);
        }
    }
}
